#!/bin/bash

polymer build

rm -rf _server/html/*
cp -r build/unbundled/* _server/html/

sed -i -e 's/192.168.0.11/192.168.0.14/g' _server/html/src/global-config.html
