FROM alpine:3.4

COPY _server/app /server
COPY _server/html /html

CMD /server
